FROM tutum/apache-php
MAINTAINER Petr Tomášek <petas.tomasek@gmail.com>

RUN apt-get update && rm -rf /var/lib/apt/lists/*
RUN rm -fr /app

ENV ALLOW_OVERRIDE true

ADD ./shop /app