$(document).ready(function() {
  $("#mainCarousel").carousel({
    interval: 4000
  });

  var clickEvent = false;
  $("#mainCarousel")
    .on("click", ".nav a", function() {
      clickEvent = true;
      $(".nav li").removeClass("active");
      $(this)
        .parent()
        .addClass("active");
    })
    .on("slid.bs.carousel", function(e) {
      if (!clickEvent) {
        var count = $(".nav").children().length - 1;
        var current = $(".nav li.active");
        current
          .removeClass("active")
          .next()
          .addClass("active");
        var id = parseInt(current.data("slide-to"));
        if (count == id) {
          $(".nav li")
            .first()
            .addClass("active");
        }
      }
      clickEvent = false;
    });
});

$(".product-carousel, .tip-carousel").slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 6,
  slidesToScroll: 6,
  responsive: [
    {
      breakpoint: 1400,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4
      }
    },
    {
      breakpoint: 1124,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
